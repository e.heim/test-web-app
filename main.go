package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"regexp"
	"time"
)

func handler(w http.ResponseWriter, r *http.Request) {
	name, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	var addresses []string
	regexp, _ := regexp.Compile("^(fe80.*)|(127.*)")

	ifaces, err := net.Interfaces()
	if err != nil {
		panic(err)
	}
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			panic(err)
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if !regexp.MatchString(ip.String()) {
				addresses = append(addresses, ip.String())
			}

		}
	}

	datetime := time.Now()
	fmt.Fprintf(w, "Hi there, I'm running on %s ! \n", name)
	fmt.Fprintf(w, "I'm reachable via this IP-Addresses: %s!\n", addresses)
	fmt.Fprintf(w, "It's %s within my container", datetime.Local())
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":80", nil))
}

[![License](https://img.shields.io/badge/licens-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/e.heim/test-web-app)](https://goreportcard.com/report/gitlab.com/e.heim/test-web-app)
<br />
# test web app
go app that spinns up a simple web server listen on port 80 (designed to run in docker).<br />
that displays the container hostname an its IPs (drops 'fe80' addresses).<br />